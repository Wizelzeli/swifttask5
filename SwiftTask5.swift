// Создать класс родитель и 2 класса наследника

class Car {
	
}

class Bmv: Car {

}

class Audi: Car {
	
}

// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)

class House {
	var width: Double
	var height: Double

	func create() -> Int {
		return width * height
	}

	func destroy() {
		print("Дом уничтожен")
	}
}

// Создайте класс с методами, которые сортируют массив учеников по разным параметрам

class Student {
    var name: String
    var mathMark: Int
    var phisycMark: Int
    var informaticMark: Int

    init(name: String, mathMark: Int, phisycMark: Int, informaticMark: Int) {
    	self.name = name
    	self.mathMark = mathMark
    	self.phisycMark = phisycMark
    	self.informaticMark = informaticMark
    }
}

var student1 = Student(name: "Mark", mathMark: 3, phisycMark: 4, informaticMark: 5)
var student2 = Student(name: "Alex", mathMark: 5, phisycMark: 4, informaticMark: 5)
var student3 = Student(name: "Toxa", mathMark: 4, phisycMark: 3, informaticMark: 5)

var studentList = [student1, student2, student3]

class StudentSorting {
	static func sortByMathMark() {
		studentList = studentList.sorted(by: {$0.mathMark > $1.mathMark})
	}

	static func sortByPhisycMark() {
		studentList = studentList.sorted(by: {$0.phisycMark > $1.phisycMark})
	}

	static func sortByInformaticMark() {
		studentList = studentList.sorted(by: {$0.informaticMark > $1.informaticMark})
	}
}

// Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов

class SomeClass { }

struct SomeStruct { }

/* 
Класс отличается от структуры тем, что:
- передаются не по значению, а по ссылке
- имеют деинициализатор для высвобождения ресурсов
- могут наследовать другие классы
*/
